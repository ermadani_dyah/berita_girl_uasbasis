<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <?php include("includes/head.php")?>
  <body>
    <!---NavBar--->
    <?php include("includes/navbar.php")?>
    <!---Silder--->
    <div class="wrap2">
      <div class="container">
        <div class="banner"> <img src="images/banner-img.jpg" alt="banner" />
          <div class="banner-shadows"></div>
        </div>
        <div class="clearing"></div>
      </div>
    </div>
    <!---Trending--->
    <div class="wrap1 ">
      <div class="container">
        <div class="title">
          <h1>Trending</h1>
        </div>
        <?php
            require 'admin/config_trending.php';
            $trending_news = $collection->find(array('colom'=>'right'));
            foreach ($trending_news as $trd) {
              $Kode_berita=$trd->kode_berita;
              $judul=$trd->judul;
              $berita=$trd->berita;
              $link=$trd->link;
              $tanggal=$trd->tanggal;
              $kategori=$trd->kategori;?>
                  <div class="box mar-right40">
                    <div class="title">
                      <h2><?php echo $kategori?></h2>
                      <h1><?php echo $judul?></h1>
                    </div>
                    <div class="content">
                      <p><?php echo $berita?></p>
                      <div class="button">
                        <a href="<?php echo $link?>">More Info </a>
                      </div>
                    </div>
                  </div>
          <?php 
            }
          ?>
          <?php
            require 'admin/config_trending.php';
            $trending = $collection->find(array('colom'=>'left'));
            foreach ($trending as $trd1) {
              $Kode_berita=$trd1->kode_berita;
              $judul=$trd1->judul;
              $berita=$trd1->berita;
              $link=$trd1->link;
              $tanggal=$trd1->tanggal;
              $kategori=$trd1->kategori;?>
                  <div class="box">
                    <div class="title">
                      <h2><?php echo $kategori?></h2>
                      <h1><?php echo $judul?></h1>
                    </div>
                    <div class="content">
                      <p><?php echo $berita?></p>
                      <div class="button">
                        <a href="<?php echo $link?>">More Info </a>
                      </div>
                    </div>
                  </div>
          <?php 
            }
          ?>
        <div class="clearing"></div>  
      </div>
      <div class="clearing"></div>
    </div>
    <!---Weekly Top News--->
    <div class="wrap4">
      <div class="container">
        <div class="title">
          <h1>Weekly Top News</h1>
        </div>
        <?php
            require 'admin/config_weekly.php';
            $weekly = $collection->find();
            foreach ($weekly as $wky) {
              $Kode_berita=$wky->kode_berita;
              $judul=$wky->judul;
              $berita=$wky->berita;
              $link=$wky->link;
              $tanggal=$wky->tanggal;
              $kategori=$wky->kategori;?>
                <div class="panel borderbotm-none">
                  <div class="content">
                    <h2><?php echo $judul?></h2>
                    <p><?php echo $berita?></p>
                    <br>
                    <time class="col-xs-6 text-right"><?php echo $tanggal?></time>
                    <div class=" button">
                      <a href="<?php echo $link?>">More Info</a>
                    </div>
                  </div>
                </div>
                <br><br>  
          <?php 
            }
          ?>
        <div class="clearing"></div>  
      </div>
    </div>
    <br><br>
    <!---FOOTER--->
    <?php include("includes/footer.php")?>
  </body>
</html>
