<!DOCTYPE html>
<html dir="ltr" lang="en">
    <?php include("includes/head.php")?>
    <body>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
            data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php include("includes/header.php")?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php include("includes/sidebar.php")?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
				<!-- ============================================================== -->
				<!-- Bread crumb and right sidebar toggle -->
				<!-- ============================================================== -->
				<div class="page-breadcrumb">
					<div class="row align-items-center">
						<div class="col-md-6 col-8 align-self-center">
							<h3 class="page-title mb-0 p-0">
								<i class="mr-3 fas fa-plus"aria-hidden="true"></i>
								Add News
							</h3>
							<div class="d-flex align-items-center">
								<nav aria-label="breadcrumb">
									<ol class="breadcrumb">
										<li class="breadcrumb-item">
                                            <a href="#">Home</a>
                                        </li>
										<li class="breadcrumb-item active" aria-current="page">All News</li>
										<li class="breadcrumb-item active" aria-current="page">Add News</li>
									</ol>
								</nav>
							</div>
						</div>
						<div class="col-md-6 col-4 align-self-center">
							<div class="text-right upgrade-btn">
								<a href="all.php"class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
									<i class="mr-3  fas fa-arrow-left"aria-hidden="true"></i>
									Kembali
								</a>
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- End Bread crumb and right sidebar toggle -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Container fluid  -->
				<!-- ============================================================== -->
				<div class="container-fluid">
					<!-- ============================================================== -->
					<!-- Start Page Content -->
					<!-- ============================================================== -->
					<div class="row">
						<!-- column -->
						<div class="col-sm-12">
							<div class="card">
								<div class="row">
									<!-- column -->
									<div class="col-sm-12">
										<div class="card-body">
											<section class="content">
												<div class="card card-info">
													<div class="card-header">
														<h3 class="card-title" style="margin-top:5px;">
															<i class="far fa-list-alt"></i> Form Add News
														</h3>
													</div>
													<!-- /.card-header -->
													<!-- form start -->
													<br>
													<form class="form-horizontal" enctype="multipart/form-data" method="post" action="konfirmasi_tambah_news.php">
														<div class="card-body">
															<div class="form-group row">
																<label for="hobi" class="col-sm-3 col-form-label">Kode Berita</label>
																<div class="col-sm-7">
																	<input type="text" name="kode_berita"  class="form-control" id="kode_berita" value="">
																</div>
															</div>
															<div class="form-group row">
																<label for="hobi" class="col-sm-3 col-form-label">Judul Berita</label>
																<div class="col-sm-7">
																	<input type="text" name="judul" class="form-control" id="judul" value="">
																</div>
															</div>
															<div class="form-group row">
                                                                <label for="hobi" class="col-sm-3 col-form-label">Berita</label>
																<div class="col-sm-7">
                                                                    <textarea class="form-control" id="berita" name="berita" cols="30" rows="7" value=""></textarea>
																</div>
															</div>
															<div class="form-group row">
                                                                <label for="hobi" class="col-sm-3 col-form-label">link/Read More</label>
																<div class="col-sm-7">
																	<input type="text" name="link"  class="form-control" id="link" value="">
																</div>
                                                            </div>
															<div class="form-group row">
                                                                <label for="hobi" class="col-sm-3 col-form-label">Tanggal</label>
																<div class="col-sm-7">
																	<input type="text" name="tanggal"  class="form-control" id="tanggal" value="">
																</div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="hobi" class="col-sm-3 col-form-label">Kategori</label>
																<div class="col-sm-7">
																	<input type="text" name="kategori"  class="form-control" id="kategori" value="">
																</div>
                                                            </div>
														</div>
														<!-- /.card-body -->
														<div class="col-sm-10">
															<button type="submit" name="submit" class="btn btn-info float-right">
																<i class="mr-3 fas fa-plus"aria-hidden="true"></i>Tambah
															</button>
														</div>  
														<!-- /.card-footer -->
													</form>
												</div>
												<!-- /.card -->
											</section>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- ============================================================== -->
					<!-- End PAge Content -->
					<!-- ============================================================== -->
					<!-- ============================================================== -->
					<!-- Right sidebar -->
					<!-- ============================================================== -->
					<!-- .right-sidebar -->
					<!-- ============================================================== -->
					<!-- End Right sidebar -->
					<!-- ============================================================== -->
				</div>
				<!-- ============================================================== -->
				<!-- End Container fluid  -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- footer -->
				<!-- ============================================================== -->
				<?php include("includes/footer.php")?>
				<!-- ============================================================== -->
				<!-- End footer -->
				<!-- ============================================================== -->
			</div>
            <!-- ============================================================== -->
            <!-- End Page wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
        <?php include("includes/script.php")?>
    </body>
</html>