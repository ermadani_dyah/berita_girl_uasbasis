<?php 
    session_start(); 
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <?php include("includes/head.php")?>
    <body>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
            data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php include("includes/header.php")?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php include("includes/sidebar.php")?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
				<!-- ============================================================== -->
				<!-- Bread crumb and right sidebar toggle -->
				<!-- ============================================================== -->
				<div class="page-breadcrumb">
					<div class="row align-items-center">
						<div class="col-md-6 col-8 align-self-center">
							<h3 class="page-title mb-0 p-0">
								Trending News
							</h3>
							<div class="d-flex align-items-center">
								<nav aria-label="breadcrumb">
									<ol class="breadcrumb">
										<li class="breadcrumb-item"><a href="#">Home</a></li>
										<li class="breadcrumb-item active" aria-current="page">Trending News</li>
									</ol>
								</nav>
							</div>
						</div>
						<div class="col-md-6 col-4 align-self-center">
							<div class="text-right upgrade-btn">
								<a href="tambah_trending.php"class="btn btn-success text-white">
									<i class="mr-3 far fa-plus-square"aria-hidden="true"></i>
									Add Trending News 
								</a>
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- End Bread crumb and right sidebar toggle -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Container fluid  -->
				<!-- ============================================================== -->
				<div class="container-fluid">
					<!-- ============================================================== -->
					<!-- Start Page Content -->
					<!-- ============================================================== -->
					<div class="row">
						<!-- column -->
						<div class="col-sm-12">
							<div class="card">
								<div class="row">
									<!-- column -->
									<div class="col-sm-12">
										<div class="card-body">
											<div class="card-header">
												<h3 class="card-title" style="margin-top:5px;">
													<i class="mr-3 fas fa-clipboard-list"aria-hidden="true"></i> List Trending News
												</h3>
											</div> 
											<div class="card-body">
                                                <div class="col-sm-12">
                                                    <?php if(!empty($_GET['notif'])){
                                                            if($_GET['notif']=="tambahberhasil"){?>
                                                                <div class="alert alert-success" role="alert">
                                                                    Data Berhasil Ditambahkan
                                                                </div>
                                                    <?php   }else if($_GET['notif']=="editberhasil"){?>
                                                                <div class="alert alert-success" role="alert">
                                                                    Data Berhasil DiEdit
                                                                </div>
                                                    <?php   }else{?>
                                                                <div class="alert alert-danger" role="alert">
                                                                    Data Berhasil Di hapus
                                                                </div>
                                                    <?php
                                                            } 
                                                        }
                                                    ?>
                                                </div>
												<table class="table table-bordered">
													<thead>                  
														<tr>
															<th width="5%">NO</th>
															<th width="20%">Tanggal</th>
															<th width="20%">Judul Berita</th>
															<th width="15%">
																<center>Aksi</center>
															</th>
														</tr>
													</thead>
													<tbody>
                                                    <?php
                                                        require 'config_trending.php';
                                                        $trending_berita = $collection->find();
                                                        $no=1;
                                                        foreach ($trending_berita as $trd) {
                                                            echo "<tr>";
                                                                echo "<td>".$no."</td>";
                                                                echo "<td>".$trd->tanggal."</td>";
                                                                echo "<td>".$trd->judul."</td>";
                                                                echo "<td align='center'>";
                                                                    echo "<a href = 'edit_trending.php?id=".$trd->_id."'class='btn btn-xs btn-info'>
                                                                            <i class='fas fa-edit'></i>
                                                                            Edit
                                                                        </a>";
                                                                    echo "<a href ='hapus_trending.php?id=".$trd->_id."' class='btn btn-xs btn-danger'>
                                                                        <i class='fas fa-trash'></i>
                                                                        Hapus
                                                                        </a>";
                                                                    echo"<a href = 'detail_trending.php?id=".$trd->_id."' class='btn btn-xs btn-warning'>
                                                                            <i class='fas fa-eye'></i>
                                                                            Detail
                                                                        </a>";
                                                            echo "</td>";
                                                            echo "</tr>";
                                                            $no++;
                                                        }
                                                    ?>
													</tbody>
												</table>
											</div>
											<!-- /.card-body -->
											<div class="card-footer clearfix">
												<span>Showing entries</span>
												<ul class="pagination pagination-sm m-0 float-right">
													
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- ============================================================== -->
					<!-- End PAge Content -->
					<!-- ============================================================== -->
					<!-- ============================================================== -->
					<!-- Right sidebar -->
					<!-- ============================================================== -->
					<!-- .right-sidebar -->
					<!-- ============================================================== -->
					<!-- End Right sidebar -->
					<!-- ============================================================== -->
				</div>
				<!-- ============================================================== -->
				<!-- End Container fluid  -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- footer -->
				<!-- ============================================================== -->
				<?php include("includes/footer.php")?>
				<!-- ============================================================== -->
				<!-- End footer -->
				<!-- ============================================================== -->
			</div>
            <!-- ============================================================== -->
            <!-- End Page wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
        <?php include("includes/script.php")?>
    </body>
</html>