<aside class="left-sidebar sidebar-dark-primary elevation-4" data-sidebarbg="skin6">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li class="sidebar-item"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashbord.php" aria-expanded="false">
                        <i class="mr-3 fas fa-align-justify" aria-hidden="true"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="all.php" aria-expanded="false">
                        <i class="mr-3 fas fa-globe"aria-hidden="true"></i>
                        <span class="hide-menu">All News</span>
                    </a>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="trending.php" aria-expanded="false">
                        <i class="mr-3 fas fa-chart-line"aria-hidden="true"></i>
                        <span class="hide-menu">News Trending</span>
                    </a>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="weekly.php" aria-expanded="false">
                        <i class="mr-3 fas fa-trophy"aria-hidden="true"></i>
                        <span class="hide-menu">Top Week News</span>
                    </a>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link"href="../index.php" aria-expanded="false">
                        <i class="mr-3 fas fa-sign-out-alt"aria-hidden="true"></i>
                        <span class="hide-menu">Kembali</span>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>