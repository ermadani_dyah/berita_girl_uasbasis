<?php session_start();
   require 'config.php';
   if (isset($_GET['id'])) {
    $all = $collection->findOne(['_id' => new MongoDB\BSON\ObjectID($_GET['id'])]);
    }
?>
<html dir="ltr" lang="en">
    <?php include("includes/head.php")?>
	<body>
		<!-- ============================================================== -->
		<!-- Preloader - style you can find in spinners.css -->
		<!-- ============================================================== -->
		<div class="preloader">
			<div class="lds-ripple">
				<div class="lds-pos"></div>
				<div class="lds-pos"></div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- Main wrapper - style you can find in pages.scss -->
		<!-- ============================================================== -->
		<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
			<!-- ============================================================== -->
			<!-- Topbar header - style you can find in pages.scss -->
			<!-- ============================================================== -->
			<?php include("includes/header.php")?>
			<!-- ============================================================== -->
			<!-- End Topbar header -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- Left Sidebar - style you can find in sidebar.scss  -->
			<!-- ============================================================== -->
			<?php include("includes/sidebar.php")?>
			<!-- ============================================================== -->
			<!-- End Left Sidebar - style you can find in sidebar.scss  -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- Page wrapper  -->
			<!-- ============================================================== -->
			<div class="page-wrapper">
				<!-- ============================================================== -->
				<!-- Bread crumb and right sidebar toggle -->
				<!-- ============================================================== -->
				<div class="page-breadcrumb">
					<div class="row align-items-center">
						<div class="col-md-6 col-8 align-self-center">
							<h3 class="page-title mb-0 p-0">
								<i class="mr-3 fas fa-pencil-alt"aria-hidden="true"></i>
								Detail News
							</h3>
							<div class="d-flex align-items-center">
								<nav aria-label="breadcrumb">
									<ol class="breadcrumb">
										<li class="breadcrumb-item">
											<a href="#">Home</a>
										</li>
										<li class="breadcrumb-item active" aria-current="page">Data News</li>
										<li class="breadcrumb-item active" aria-current="page">Detail Data News</li>
									</ol>
								</nav>
							</div>
						</div>
						<div class="col-md-6 col-4 align-self-center">
							<div class="text-right upgrade-btn">
								<a href="all.php" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
									<i class="mr-3  fas fa-arrow-left"aria-hidden="true"></i>
									Kembali
								</a>
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- End Bread crumb and right sidebar toggle -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Container fluid  -->
				<!-- ============================================================== -->
				<div class="container-fluid">
					<!-- ============================================================== -->
					<!-- Start Page Content -->
					<!-- ============================================================== -->
					<div class="row">
						<!-- column -->
						<div class="col-sm-12">
							<div class="card">
								<div class="row">
									<!-- column -->
									<div class="col-sm-12">
										<div class="card-body">
											<section class="content">
												<div class="card card-info">
													<div class="card-header">
														<h3 class="card-title" style="margin-top:5px;">
															<i class="far fa-list-alt"></i> Form Data News
														</h3>
													</div>
													<!-- /.card-header -->
													<!-- form start -->
													<br>
													<table class="table table-bordered">
														<tbody>  
														   <tr>
																<td colspan="2">
																	<i class=" fas fa-search-plus"></i> 
																	<strong>Detail News</strong>
																</td>
														   </tr>               
														   <tr>
																<td width="20%">
																	<strong>Kode Berita</strong>
																</td>
																<td width="80%">
                                                                <?php echo "$all->kode_berita"; ?>
																</td>
															</tr>                 
															<tr>
																<td width="20%">
																	<strong>Judul Berita</strong>
																</td>
																<td width="80%">
                                                                    <?php echo "$all->judul"; ?>
																</td>
															</tr>                 
															<tr>
																<td width="20%">
																	<strong>Berita</strong>
																</td>
																<td width="80%">
                                                                    <?php echo "$all->berita"; ?>
																</td>
															</tr> 
															<tr>
																<td>
																	<strong>Link/Read More<strong>
																</td>
																<td width="80%">
                                                                    <?php echo "$all->link"; ?>
																</td>
															</tr>
															<tr>
																<td>
																	<strong>Tanggal<strong>
																</td>
																<td width="80%">
                                                                    <?php echo "$all->tanggal"; ?>
																</td>
															</tr>
															<tr>
																<td>
																	<strong>Kategori<strong>
																</td>
																<td width="80%">
                                                                    <?php echo "$all->kategori"; ?>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<!-- /.card -->
											</section>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- ============================================================== -->
					<!-- End PAge Content -->
					<!-- ============================================================== -->
					<!-- ============================================================== -->
					<!-- Right sidebar -->
					<!-- ============================================================== -->
					<!-- .right-sidebar -->
					<!-- ============================================================== -->
					<!-- End Right sidebar -->
					<!-- ============================================================== -->
				</div>
				<!-- ============================================================== -->
				<!-- End Container fluid  -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- footer -->
				<!-- ============================================================== -->
				<?php include("includes/footer.php")?>
				<!-- ============================================================== -->
				<!-- End footer -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- End Page wrapper  -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- End Wrapper -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- All Jquery -->
		<!-- ============================================================== -->
		<?php include("includes/script.php")?>
	</body>
</html>