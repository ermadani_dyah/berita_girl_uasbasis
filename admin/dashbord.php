<!DOCTYPE html>
<html dir="ltr" lang="en">
    <?php include("includes/head.php")?>
    <body>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
            data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php include("includes/header.php")?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php include("includes/sidebar.php")?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="page-breadcrumb">
                    <div class="row align-items-center">
                        <div class="col-md-6 col-8 align-self-center">
                            <h3 class="page-title mb-0 p-0">Dashboard</h3>
                            <div class="d-flex align-items-center">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Sales chart -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- Column -->
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Pembaca Perhari</h4>
                                    <div class="text-right">
                                        <h2 class="font-light m-b-0">
                                            <i class="ti-arrow-up text-success"></i>
                                        </h2>
                                        <span class="text-muted">Kenaikan</span>
                                    </div>
                                    <span class="text-success">80%</span>
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 80%; height: 6px;" aria-valuenow="25" aria-valuemin="0"aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Pembaca PerMinggu</h4>
                                    <div class="text-right">
                                        <h2 class="font-light m-b-0">
                                            <i class="ti-arrow-up text-info"></i>
                                        </h2>
                                        <span class="text-muted">Kenaikan</span>
                                    </div>
                                    <span class="text-info">30%</span>
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar"style="width: 30%; height: 6px;" aria-valuenow="25" aria-valuemin="0"aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                    </div>
                    <!-- ============================================================== -->
                    <!-- Sales chart -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Table -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <h4 class="card-title col-md-10 mb-md-0 mb-3">Anggota Kelompok:</h4>
                                    </div>				
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- Table -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Recent blogss -->
                        <!-- ============================================================== -->
                        <div class="row justify-content-center">
                            <!-- Column -->
                            <div class="col-lg-4 col-md-6">
                                <div class="card">
                                    <img class="card-img-top img-responsive" src="assets/images/big/angel2.jpg" alt="Card">
                                    <div class="card-body">
                                        <h3 class="font-normal">Jane Angel B.N</h3>
                                        <p>
                                            <i class="mr-3   fas fa-id-badge"aria-hidden="true"></i>
                                            193140914111001
                                        </p>
                                        <p>
                                            <i class="mr-3 fab fa-whatsapp"aria-hidden="true"></i>
                                            08165456667
                                        </p>
                                        <p>
                                            <i class="mr-3 fas fa-envelope"aria-hidden="true"></i>
                                            janeangelbrilian@gmail.com
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-lg-4 col-md-6">
                                <div class="card">
                                    <img class="card-img-top img-responsive" src="assets/images/big/dd2.jpg" alt="Card">
                                    <div class="card-body">
                                        <h3 class="font-normal">ERMADANI DYAH R</h3>
                                        <p>
                                            <i class="mr-3   fas fa-id-badge"aria-hidden="true"></i>
                                            193140914111016
                                        </p>
                                        <p>
                                            <i class="mr-3 fab fa-whatsapp"aria-hidden="true"></i>
                                            081555442712
                                        </p>
                                        <p>
                                            <i class="mr-3 fas fa-envelope"aria-hidden="true"></i>
                                            ermadani.dyah09@gmail.com
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- Recent blogss -->
                    <!-- ============================================================== -->
                </div>
                <!-- ============================================================== -->
                <!-- End Container fluid  -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- footer -->
                <!-- ============================================================== -->
                <?php include("includes/footer.php")?>
                <!-- ============================================================== -->
                    <!-- End footer -->
                    <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Page wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
        <?php include("includes/script.php")?>
    </body>
</html>