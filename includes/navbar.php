<div class="wrap1">
    <div class="container">
        <div class="header">
            <div class="logo">
                <h1>Fashion and Beauty</h1>
            </div>
            <div class="menu">
                <ul class="sf-menu" id="example">
                    <li>
                        <a href="index.php">Home</a>
                    </li>
                    <li class="current"> 
                        <a href="#">Fashion</a>
                        <ul>
                            <li> 
                                <a href="style.php">Style and Trends</a> 
                            </li>
                            <li> 
                                <a href="look.php">look for less</a>
                            </li>
                        </ul>
                    </li>
                    <li class="current"> 
                        <a href="#">Beauty</a>
                        <ul>
                            <li> 
                                <a href="hair.php">Hair</a> 
                            </li>
                            <li> 
                                <a href="skin.php">Skin</a>
                            </li>
                            <li> 
                                <a href="makeup.php">Make Up</a>
                            </li>
                            <li> 
                                <a href="healty.php">Healty</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="all.php">All News</a>
                    </li>
                    <li>
                        <a href="admin/dashbord.php">Admin</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="clearing"></div>
    </div>
    <div class="clearing"></div>
</div>