<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Fashion and Beauty</title>
    <link href="css/styles.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Economica' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <!----menu--->
    <link rel="stylesheet" href="css/superfish.css" media="screen">
    <script src="js/jquery-1.9.0.min.js"></script>
    <script src="js/hoverIntent.js"></script>
    <script src="js/superfish.js"></script>
    <script>
        // initialise plugins
        jQuery(function(){
            jQuery('#example').superfish({
                //useClick: true
            });
         });
    </script>
</head>