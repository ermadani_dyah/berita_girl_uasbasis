<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <?php include("includes/head.php")?>
  <body>
    <!---NavBar--->
    <?php include("includes/navbar.php")?>
    <!---NEWS STYLE--->
    <div class="wrap4">
      <div class="container">
        <div class="leftcol">
          <div class="title">
            <h1>Hair</h1>
          </div>
          <?php
            require 'admin/config.php';
            $all_news = $collection->find(array('kategori'=>'hair'));
            foreach ($all_news as $all) {
              $Kode_berita=$all->kode_berita;
              $judul=$all->judul;
              $berita=$all->berita;
              $link=$all->link;
              $tanggal=$all->tanggal;
              $kategori=$all->kategori;?>
                <div class="panel">
                  <div class="content">
                    <h2><?php echo $judul?></h2>
                    <p><?php echo $berita?></p>
                    <br>
                    <time class="col-xs-6 text-right"><?php echo $tanggal?></time>
                    <div class=" button">
                      <a href="<?php echo $link?>">More Info</a>
                    </div>
                  </div>
              </div>
          <?php 
            }
          ?>
        </div>
        <div class="rightcol">
          <div class="title">
            <h1>ALL NEWS</h1>
          </div>
          <div class="panel">
            <div class="content">
            <?php
              require 'admin/config.php';
              $all_news = $collection->find();
              foreach ($all_news as $all) {
                $Kode_berita=$all->kode_berita;
                $judul=$all->judul;
                $berita=$all->berita;
                $link=$all->link;
                $tanggal=$all->tanggal;
                $kategori=$all->kategori;?>
                    <ul>
                      <li>
                        <a href="<?php echo $link?>"><?php echo $judul?></a>
                      </li>
                    </ul>
            <?php
              }
            ?>
              
            </div>
          </div>
        </div>
        <div class="clearing"></div>  
      </div>
    </div>
    <br><br>
    <!---FOOTER--->
    <?php include("includes/footer.php")?>
  </body>
</html>